#ifndef STACK_HPP
#define STACK_HPP

#include <iostream>

using namespace std;

template<class T>
class Stack{
private:
  size_t capacity;
  size_t size;
  T** data;
public:
  Stack(size_t capacity);
  void pop(T** S);
  void push();
  T* next();
  bool is_empty();
  ~Stack();
};

template<class T> Stack<T>::Stack(size_t c) {
  capacity = c;
  data = new T*[capacity];
  for(size_t i = 0; i < capacity; ++ i) {
    data[i] = new T;
  }
  size = 0;
}


template<class T>
inline void Stack<T>::pop(T** S) {
  swap(*S, data[-- size]);
}

template<class T>
inline void Stack<T>::push() {
  size ++;
  assert(size <= capacity);
}

template<class T>
inline T* Stack<T>::next() {
  return data[size];
}

template<class T>
inline bool Stack<T>::is_empty() {
  return size==0;
}

template<class T>
Stack<T>::~Stack() {
  for(size_t i = 0; i < capacity; ++ i) {
    delete data[i];
  }
  delete[] data;
}

#endif
