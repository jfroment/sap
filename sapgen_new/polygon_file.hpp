#ifndef POLYGON_FILE_HPP
#define POLYGON_FILE_HPP

#include "bit_fstream.hpp"


class PolygonFileOutput {
private:
  string name;
  size_t number;
  size_t length;
  Polygon last;
  BitOutputFstream file;
public:
  PolygonFileOutput(size_t length, string filename);
  void write(const Polygon& p);
  void write2(const Polygon& p);
  void close();
};


class PolygonFileInput {
private:
  size_t number;
  size_t length;
  Polygon last;
  BitInputFstream file;
public:
  PolygonFileInput(size_t length, string filename);
  void read(Polygon& P);
  void read2(Polygon& P);
  void close();
  size_t get_number() const;
};

inline PolygonFileOutput::PolygonFileOutput(size_t l, string filename) {
  length = l;
  number = 0;
  last.set_length(l);
  file.open(filename); 
}

inline void PolygonFileOutput::write2(const Polygon& p) {
  int lp = p.common_prefix(last);
  file.write_int(lp, 6);
  ++ number;
  for (int i = lp; i < length - 1; ++ i) {
    file.write_int(p[i], 2);
  }
  last = p;
}

inline void PolygonFileOutput::write(const Polygon& p) {
  ++ number;
  for (int i = 1; i < length - 1; ++ i) {
    file.write_int(p[i], 2);
  }
}

inline void PolygonFileOutput::close() {
  file.complete();
  file.write_size_t(number);
  file.close();
}

inline PolygonFileInput::PolygonFileInput(size_t l, string filename) {
  length = l;
  file.open(filename);
  file.end();
  number = file.read_size_t();
  file.begin();
  last.set_length(l);
}

inline void PolygonFileInput::read(Polygon& P) {
  P[0] = Right;
  P[length - 1] = Down;
  for (int i = 1; i < length - 1; ++ i) {
    P[i] = (Step)file.read_int(2);
  }
}

inline void PolygonFileInput::read2(Polygon& P) {
  P = last;
  int lp = file.read_int(6);
  for (int i = lp; i < length - 1; ++ i) {
    P[i] = (Step)file.read_int(2);
  }
  last = P;
}

inline size_t PolygonFileInput::get_number() const{
  return number;
}

inline void PolygonFileInput::close() {
  file.close();
}

#endif
