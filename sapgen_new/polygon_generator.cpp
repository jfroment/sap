#include "polygon_generator.hpp"

void PolygonGenerator::init_grid(int l) {
  assert(l < maximum_length);
  length = l;
  grid_width = length;
  grid_height = length / 2 + 2;
  x_start = 1 + (length - 6) / 2;
  //cout << "xstart = " << x_start << endl;
  for (int x = 0; x < grid_width; ++ x) {
    for (int y = 0; y < grid_height; ++ y) {
      init_cell(x, y);
    }
  }
}

void PolygonGenerator::init_cell(int x, int y) {
  Cell& cell = grid[pos(x, y)];
  if (on_border(x, y)) {
    cell.distance = 65535;
  }
  else{
    int dx = x - x_start;
    int dy = y - y_end;
    dx = (dx < 0) ? -dx : dx;
    dy = (dy < 0) ? -dy : dy;
    cell.distance = dx + dy;
    cell.timestamp = -1; // No timestamp
  }
}

bool PolygonGenerator::on_border(int x, int y) {
  if (x == 0 or x == grid_width - 1 or y == 0 or y == grid_height - 1) return true;
  if (y == 1 and x < x_start) return true;
  return false;
}

/*void PolygonGenerator::display() const {
  for (int y = grid_height - 1; y >=0; -- y) {
    for (int x = 0; x < grid_width * 5 + 1; ++ x) cout << '-';
    cout << endl;
    for (int z = 0; z < 3; ++ z) {
      for (int x = 0; x < grid_width; ++ x) {
	cout << '|';
	Cell cell = grid[pos(x, y)];
	if (cell.raw == border_cell_raw) cout << "####";
	else {
	  if (z == 0) {
	    Position p = pos(x,y);
	    if (p < 1000) cout << ' ';
	    if (p < 100) cout << ' ';
	    if (p < 10) cout << ' ';
	    cout << p; 
	  }
	  else if (z == 1) {
	    Timestamp t = grid[pos(x,y)].info.timestamp;
	    if (t == 65535 ) cout << " -1 ";
	    else{
	      if (t < 1000) cout << ' ';
	      if (t < 100) cout << ' ';
	      if (t < 10) cout << ' ';
	      cout << t;
	    }
	  }
	  else{
	    Distance d = grid[pos(x,y)].info.distance;
	    if (d < 10) cout << ' ';
	    cout << d << ' ' << to_char(grid[pos(x,y)].info.step);
	  }
	}
      }
      cout << '|'<< endl; 
    }
  }
  }*/

void PolygonGenerator::init_exploration() {
  Position p = pos(x_start, y_start);
  add_to_stack(0, p, right(p), Right); 
}


bool PolygonGenerator::next() {
  while(not stack.is_empty()) {
    stack.pop(&current_stack_info);
    Cell& cell_info= grid[current_stack_info -> before];
    Timestamp timestamp = current_stack_info -> timestamp;
    cell_info.timestamp = timestamp;
    histo[timestamp] = current_stack_info -> before;
    histo[timestamp + 1] = 65535;
    Step s = current_stack_info -> step;
    cell_info.step = s;
    polygon[timestamp] = s;
    if (timestamp == length - 1 and cell_info.distance == 0) {
      return true;
    }
    // Add possible next step to the stack
    add_possible_next_steps_from(current_stack_info -> after, timestamp + 1);
  }
  return false;
}

void PolygonGenerator::add_possible_next_steps_from(Position p, Timestamp t) {
  //cout << "Try add from " << p << "@ " << t << endl;
  Position u = up(p);
  Position r = right(p);
  Position l = left(p);
  Position d = down(p);

  if (is_valid(u, t + 1)) add_to_stack(t, p, u, Up);
  if (is_valid(r, t + 1)) add_to_stack(t, p, r, Right);
  if (is_valid(l, t + 1)) add_to_stack(t, p, l, Left);
  if (is_valid(d, t + 1)) add_to_stack(t, p, d, Down);
}

bool PolygonGenerator::is_valid(Position p, Timestamp t) {
  Cell& cell = grid[p];
  Timestamp ct = cell.timestamp;
  if (ct == 0 and t == length) return true;
  if (ct < t and histo[ct] == p) return false;
  return t + cell.distance <= length;
}

void PolygonGenerator::add_to_stack(Timestamp t, Position b, Position a, Step s) {
  StackInfo* stack_info = stack.next();
  stack_info -> timestamp = t;
  stack_info -> before = b;
  stack_info -> after = a;
  stack_info -> step = s;
  stack.push();
}
