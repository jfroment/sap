#ifndef POLYGON_HPP
#define POLYGON_HPP

#include <iostream>

using namespace std;

static const size_t maximum_length = 40;

enum Step:uint16_t {Left = 0, Right = 1, Up = 2, Down = 3};

char to_char(Step s);
class Polygon{
private:
  int length;
  Step steps[maximum_length];
  
public:

  Polygon();
  Polygon& operator=(const Polygon& p);
  void set_length(int length);
  Step& operator[](int i);
  Step operator[](int i) const;
  void display() const;
  int common_prefix(const Polygon& p) const;
};

inline char to_char(Step s) {
  switch(s){
  case Left:
    return 'L';
  case Right:
    return 'R';
  case Up:
    return 'U';
  case Down:
    return 'D';
  default:
    return '?';
    break;
  };
}

inline Polygon::Polygon() {
}

inline Polygon& Polygon::operator=(const Polygon& P) {
  length = P.length;
  for (size_t i = 0; i < length; ++ i) {
    steps[i] = P.steps[i];
  }
  return *this;
}

inline void Polygon::set_length(int l) {
  length = l;
  for(int i = 0; i < l / 2 - 1; ++ i) {
    steps[i] = Right;
    steps[i + l / 2] = Left;
  }
  steps[l / 2 - 1] = Up;
  steps[l - 1] = Down;
}

inline Step& Polygon::operator[](int i) {
  assert(0 <= i);
  assert(i< length);
  return steps[i];
}

inline Step Polygon::operator[](int i) const {
  assert(0 <= i);
  assert(i< length);
  return steps[i];
}

inline void Polygon::display() const {
  for (int i = 0; i < length; ++ i) {
    cout << to_char(steps[i]);
  }
}

inline int Polygon::common_prefix(const Polygon& P) const {
  int i;
  for (i = 0; i < length; ++ i){
    if (steps[i] != P.steps[i]) return i;
  }
  return i;
}
#endif
