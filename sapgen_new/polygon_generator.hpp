#ifndef POLYGON_GENERATOR_HPP
#define POLYGON_GENERATOR_HPP

#include <cassert>
#include <cstdint>
#include <iostream>

#include "polygon.hpp"
#include "stack.hpp"

using namespace std;



using Timestamp = uint16_t;
using Position = uint16_t;
using Distance = uint16_t;

char to_char(Step s);

class PolygonGenerator{
private:
  struct Cell{
    Distance distance;
    Timestamp timestamp;
    Step step;
  };
  struct StackInfo{
    Timestamp timestamp;
    Position before;
    Position after;
    Step step;
  };
  Position histo[maximum_length];
  Polygon polygon;
  static const int maximum_grid_width = maximum_length;
  static const int maximum_grid_height = maximum_length / 2 + 2;
  static const size_t grid_size = maximum_grid_width * maximum_grid_height;
  static const int64_t border_cell_raw = -1L;
  int length;
  int grid_width;
  int grid_height;
  Stack<StackInfo> stack;
  static const int y_start = 1;
  static const int y_end = 2;
  int x_start;
  Cell grid[grid_size];
  StackInfo* current_stack_info;
  Position pos(int x, int y) const;
  void init_cell(int x, int y);
  bool on_border(int x, int y);
  void init_exploration();
  void add_possible_next_steps_from(Position p, Timestamp t);
  static Position left(Position);
  static Position right(Position);
  static Position up(Position);
  static Position down(Position);
  bool is_valid(Position p, Timestamp t);
  void add_to_stack(Timestamp t, Position before, Position afer, Step s);
  void init_grid(int length);

public:

  PolygonGenerator(int length);
  ~PolygonGenerator();
  void set_length(int length);
  void display() const;
  bool next();
  const Polygon& get_polygon() const;

};

inline Position PolygonGenerator::pos(int x, int y) const {
  return y * maximum_grid_width + x;
}

inline PolygonGenerator::PolygonGenerator(int length): stack(3 * length) {
  current_stack_info = new StackInfo;
  set_length(length);
}

inline PolygonGenerator::~PolygonGenerator() {
  delete current_stack_info;
}

inline Position PolygonGenerator::right(Position pos) {
  return pos + 1;
}

inline Position PolygonGenerator::left(Position pos) {
  return pos - 1;
}

inline Position PolygonGenerator::up(Position pos) {
  return pos + maximum_grid_width;
}

inline Position PolygonGenerator::down(Position pos) {
  return pos - maximum_grid_width;
}

inline const Polygon& PolygonGenerator::get_polygon() const {
  return polygon;
}

inline void PolygonGenerator::set_length(int length) {
  init_grid(length);
  init_exploration();
  polygon.set_length(length);
}
#endif
