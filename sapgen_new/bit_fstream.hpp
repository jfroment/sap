#ifndef BIT_FSTREAM_HPP
#define BIT_FSTREAM_HPP


#include <fstream>

class BitOutputFstream {
private:
  static const size_t buffer_size = 8;
  static const size_t maximum_size = 8 * buffer_size;
  int size; // number of bits in the buffer 
  char buffer[buffer_size]; 
  ofstream file;
  void clear_buffer();
  void flush();
  string name;
public:
  BitOutputFstream();
  void open(string filename);
  void write(bool b);
  void write_int(int v, int n);
  void write_size_t(size_t v);
  void close();
  void complete();
  void to_begin();
};

class BitInputFstream {
private:
  static const size_t buffer_capacity = 8;
  bool eof;
  size_t buffer_size;
  int available_bits;
  int offset;
  char buffer[buffer_capacity]; 
  ifstream file;
  void fill_buffer();
public:
  BitInputFstream();
  void open(string filename);
  bool read();
  int read_int(int n);
  size_t read_size_t();
  void close();
  bool is_eof() const;
  void end();
  void begin();
};

inline BitOutputFstream::BitOutputFstream() {
}

inline void BitOutputFstream::open(string filename) {
  name = filename;
  file.open(filename.c_str(), ios::binary | ios::trunc);
  clear_buffer();
}

inline void BitOutputFstream::clear_buffer() {
  for(int i = 0; i< buffer_size; ++ i) buffer[i] = 0;
  size = 0;
}

inline void BitOutputFstream::flush() {
  size_t output_size = (size + 7) / 8;
  file.write(buffer, output_size);
  clear_buffer();
}

inline void BitOutputFstream::write(bool b) {
  if(b) buffer[size / 8] += (1 << (size % 8));
  ++ size;
  if (size == maximum_size) flush();
}

inline void BitOutputFstream::write_int(int v, int n) {
  for (int i = 0; i < n; ++ i) {
    write((v & (1 << i)) != 0);
  }
}

inline void BitOutputFstream::write_size_t(size_t v) {
  for (int i = 0; i < 8 * sizeof(size_t); ++ i) {
   write((v & (1L << i)) != 0);
  }
}

inline void BitOutputFstream::complete() {
  while(size % 8 != 0) write(false);
}

inline void BitOutputFstream::close() {
  if (size != 0) flush();
  file.close();
}

inline BitInputFstream::BitInputFstream() {
}

inline bool BitInputFstream::is_eof() const {
  return eof and (offset >= available_bits);
}

inline void BitInputFstream::open(string filename) {
  file.open(filename.c_str(), ios::binary);
  eof = false;
  available_bits = 0;
  offset = 0;
}

inline void BitInputFstream::fill_buffer() {
  file.read((char*)buffer, buffer_capacity);
  buffer_size = file.gcount();
  eof = (buffer_size < buffer_capacity);
  available_bits = 8 * buffer_size;
  offset = 0;
}


inline bool BitInputFstream::read() {
  if (offset == available_bits) fill_buffer();
  bool res = (buffer[offset / 8] & (1 << (offset % 8))) != 0;
  ++ offset;
  return res;
}


inline int BitInputFstream::read_int(int n) {
  int res = 0;
  for (int i = 0; i < n; ++ i) {
    bool b = read();
    if (b) res += (1 << i);
  }
  return res;
}

inline size_t BitInputFstream::read_size_t() {
  size_t res = 0;
  for (int i = 0; i < 8 * sizeof(size_t) ; ++ i) {
    bool b = read();
    if (b) res += (1L << i);
  }
  return res;
}

inline void BitInputFstream::close() {
  file.close();
}


inline void BitInputFstream::end() {
  file.seekg (-8, file.end);
}

inline void BitInputFstream::begin() {
  file.seekg (0, file.beg);
}
#endif
