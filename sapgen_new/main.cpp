#include "polygon_generator.hpp"
#include "polygon_file.hpp"
#include "bit_fstream.hpp"
 
int main() {
  int l = 28;
  PolygonGenerator gen(l);
  string filename = "res"+to_string(l)+"_v2.data";
  PolygonFileOutput file_out(l, filename);
  size_t i = 0;
  const Polygon& polygon = gen.get_polygon();
  int pref = 1;
  while(gen.next()){
    file_out.write(polygon);
    //cout << i << " : " ;
    //polygon.display();
    //cout << endl;
    ++ i;
  }
  file_out.close();
  cout << i << endl;

  exit(0);
  cout << " ------ " << endl;
  PolygonFileInput file_in(l, filename);
  size_t num = file_in.get_number();
  Polygon P;
  P.set_length(l);
  int j = 0;
  for(j = 0; j < num; ++ j) {
    //cout << j << " : ";
    file_in.read2(P);
    //P.display();
    //cout << endl;
  }
  cout << j << endl;

}
