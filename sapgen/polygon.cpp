#include "polygon.hpp"

size_t Polygon::max_len;
size_t Polygon::grid_width;
size_t Polygon::grid_height;
size_t Polygon::grid_size;
int64_t Polygon::xinit;

//--------------------
// Polygon::init_grid
//--------------------

void
Polygon::init_grid(size_t length){
  max_len=length;
  grid_width=max_len-1;
  grid_height=max_len/2+2;
  grid_size=grid_width*grid_height;
  xinit=max_len/2-2;
}

//---------------
// Polygon::init
//---------------

void
Polygon::init(){
  for(size_t i=0;i<grid_size;++i){
    grid[i]=empty;
    grid_histo[i]=0;
  }
  for(size_t i=0;i<grid_width;++i){
    grid[pos(i,0)]=border;
    grid[pos(i,grid_height-1)]=border;
  }
  for(size_t i=0;i<grid_height;++i){
    grid[pos(0,i)]=border;
    grid[pos(grid_width-1,i)]=border;
  }
  for(size_t i=0;i<xinit;++i){
    grid[pos(i,1)]=border;
  }
  head=pos(xinit,yinit);
  grid[head]=1;
}

//--------------------
// Polygon::operator=
//--------------------

Polygon&
Polygon::operator=(const Polygon& P){
  head=P.head;
  length=P.length;
  memcpy(grid,P.grid,grid_size*sizeof(size_t));
  memcpy(grid_histo,P.grid_histo,grid_size*sizeof(size_t));
  memcpy(step_histo,P.step_histo,(max_len+1)*sizeof(size_t));
  return *this;
}

//------------------
// Polygon::display
//------------------

void
Polygon::display() const{
    for(size_t z=0;z<grid_height;++z){
    size_t y=grid_height-z-1;
    for(size_t x=0;x<grid_width;++x){
      size_t v=grid[pos(x,y)];
      if(v==border) cout<<"\033[47m \033[0m";
      else if(v==empty or step_histo[v]!=grid_histo[pos(x,y)] or v>length) cout<<" ";
      else cout<<"\033[42m\033[30m"<<" "<<"\033[0m";
    }
    cout<<endl;
  }
}

//-------------------
// Polygon::set_code 
//-------------------

void
Polygon::set_code(size_t& a,size_t& b) const{
  a=0;
  b=0;
  size_t c=pos(xinit,yinit);
  size_t t;
  for(t=1;t<length;++t){
    size_t l=left(c);
    size_t r=right(c);
    size_t u=up(c);
    size_t d=down(c);
    size_t v;
    size_t s=step_histo[t+1];
    if(grid[l]==t+1 and s==grid_histo[l]){
      c=l;
      v=0;
    }
    else if(grid[r]==t+1 and s==grid_histo[r]){
      c=r;
      v=1;
    }
    else if(grid[u]==t+1 and s==grid_histo[u]){
      c=u;
      v=2;
    }
    else if(grid[d]==t+1 and s==grid_histo[d]){
      c=d;
      v=3;
    }
    else{
      cout<<"Erreur"<<endl;
    }
    if(t<=16){
      a=a*4+v;
    }
    else{
      b=b*4+v;
    }
  }
  if(t<=16){
    a=a*4+3;
  }
  else{
    b=b*4+3;
  }
  
}


