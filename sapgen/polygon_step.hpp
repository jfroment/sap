#ifndef POLYGON_STEP_HPP
#define POLYGON_STEP_HPP

static const size_t max_max_len=36;


struct Step{
  size_t histo[max_max_len+1];
  size_t t,oldh,newh;
  Step(size_t t,size_t oldh,size_t newh,size_t n);
  Step(Step* s,size_t newh,size_t n);
  size_t num() const;
};


inline
Step::Step(Step* s,size_t _newh,size_t n){
  memcpy(histo,s->histo,(1+s->t)*sizeof(size_t));
  t=s->t+1;
  histo[t]=n;
  oldh=s->newh;
  //cout<<"Step histo : "<<histo[0]<<endl;
  newh=_newh;
}

inline
Step::Step(size_t _t,size_t _oldh,size_t _newh,size_t n){
  t=_t;oldh=_oldh;newh=_newh;
  histo[0]=0;
  histo[1]=0;
  histo[_t]=1;
}

inline size_t
Step::num() const{
  return histo[t];
}

#endif
