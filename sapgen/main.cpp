#include <string>
#include <iostream>
#include <tclap/CmdLine.h>
#include <chrono>
#include <ctime>
#include <cmath>
#include <fstream>
#include "polygon_generator.hpp"

using namespace std::chrono;

size_t nb_sap[19]={0, 0, 1, 2, 7, 28, 124, 588, 2938, 15268, 81826, 449572, 2521270, 14385376, 83290424, 488384528, 2895432660, 17332874364, 104653427012};

int main(int argc,char** argv){
  size_t length;
  size_t size;
  try {  
    TCLAP::CmdLine cmd("Produce self avoiding polygons of a given length on the square latice.", ' ', "0.1");
    TCLAP::ValueArg<int> lengthArg("l","length","length of polygons",true,16,"integer");
    TCLAP::ValueArg<int> sizeArg("s","size","number of polygons per file",false,0,"integer");
    cmd.add(lengthArg);
    cmd.add(sizeArg);
    cmd.parse(argc,argv);
    length = lengthArg.getValue();
    size=sizeArg.getValue();
  }
  catch (TCLAP::ArgException &e){
    cerr <<"[Error] "<<e.error()<<" for arg "<<e.argId()<<endl;
    return -1;
  }
  if(length<4 or length>36){
    cerr<<"[Error] Length must be in [4,36]"<<endl;
    return -2;
  }
  if(length%2!=0){
    cerr<<"[Error] Length must be even"<<endl;
  }
  size_t total=nb_sap[length/2];
  if(size==0) size=total;
  if(size<1 or size>nb_sap[length/2]){
    cerr<<"[Error] Number of polygons per file must be in [1,"<<total<<"]"<<endl;
  }
  size_t numberFiles=ceil((float)total/(float)size);
  cout<<"\033[2J\033[1;1H";
  cout<<"Split all self avoiding polygons of length "<<length<<" in "<<numberFiles<<" files."<<endl;
  cout<<"Polygons to treat = "<<total<<endl;
  string filename="infos_"+to_string(length);
  fstream out;
  out.open(filename.c_str(),fstream::out|fstream::trunc|fstream::binary);
  out.write((char*)&length,8);
  out.write((char*)&numberFiles,8);
  out.close();
  Polygon::init_grid(length);
  PolygonGenerator gen;
  size_t n=0;
  Polygon* P;
  P=new Polygon;
  auto start=high_resolution_clock::now();
  auto last=start;
  size_t a,b;
  size_t fileid=0;
  filename="poly_"+to_string(length)+"_0";
  out.open(filename.c_str(),fstream::out|fstream::trunc|fstream::binary);
  size_t filesize=size;
  out.write((char*)&length,8);
  out.write((char*)&filesize,8);
  out.write((char*)&fileid,8);
  cout<<"Current file = "<<filename<<endl;
  cout<<"File size = "<<filesize<<endl;
  size_t infile=0;
  while(gen.next()){
    //    ++n;
    gen.set(*P);
    if(P->length==length){
      ++n;
      P->set_code(a,b);
      out.write((char*)&a,8);
      out.write((char*)&b,8);
      ++infile;
      if(infile==filesize){
	out.close();
	++fileid;
	filename="poly_"+to_string(length)+"_"+to_string(fileid);
	if(fileid<numberFiles){
	  out.open(filename.c_str(),fstream::out|fstream::trunc|fstream::binary);
	
	  if(fileid+1==numberFiles){
	    filesize=total%size;
	  }
	  out.write((char*)&length,8);
	  out.write((char*)&filesize,8);
	  out.write((char*)&fileid,8);
	}
	infile=0;
      }
    }
    auto cur=high_resolution_clock::now();
    auto elapsed=chrono::duration_cast<chrono::seconds>(cur - last);
    if(elapsed.count()>1){
      auto duration=chrono::duration_cast<chrono::milliseconds>(cur-start).count();
      last=cur;
      float speed=(float)(total/(10*duration))/100;
      int percent=(float)((n*1000)/total)/10;
      cout<<"\033[2J\033[1;1H";
      cout<<"Split all self avoiding polygons of length "<<length<<" in "<<numberFiles<<" files."<<endl;
      cout<<"Polygons to treat = "<<total<<endl;
      cout<<"Current file = "<<filename<<endl;
      cout<<"File size = "<<filesize<<endl;
      cout<<endl<<'[';
      for(int i=0;i<(percent/2);++i) cout<<'=';
      cout<<"\033[6;52H] "<<percent<<"%"<<endl<<endl;
      cout<<"Treated = "<<n<<endl;
      cout<<"Speed = "<<speed<<" millions polygons per second"<<endl;
      int rem=((total-n)*duration)/(1000*n);
      int h=rem/3600;
      int m=(rem-3600*h)/60;
      int s=(rem-3600*h-60*m);
      cout<<"Remaining time = ";
      if(h>0){
	cout<<h<<'h'<<m<<'m'<<endl;
      }
      else{
	if(m>0){
	  cout<<m<<'m';
	}
	cout<<s<<'s'<<endl;
      }
    }
  }
  cout<<n<<" of "<<total<<endl;
}
