#include <string>
#include <tclap/CmdLine.h>
#include <fstream>
#include <cassert>
#include <cmath>
#include <iomanip>


using namespace std;

size_t nb_sap[19]={0, 0, 1, 2, 7, 28, 124, 588, 2938, 15268, 81826, 449572, 2521270, 14385376, 83290424, 488384528, 2895432660, 17332874364, 104653427012};

int main(int argc,char** argv){
  int l;
  try{
    TCLAP::CmdLine cmd("Make statistics on fp coefficients.", ' ', "0.1");
    TCLAP::ValueArg<int> lengthArg("l","length","Length of self avoiding polygon",true,0, "code");
    cmd.add(lengthArg);
    cmd.parse(argc, argv);
    l=lengthArg.getValue();    
  }
  catch (TCLAP::ArgException &e){
    cerr <<"[Error] "<<e.error()<<" for arg "<<e.argId()<<endl;
    return -1;
  }

  string filename="infos_"+to_string(l);
  cout<<filename<<endl;
  fstream file;
  file.open(filename.c_str(),fstream::in|fstream::binary);
  string str;
  size_t length;
  size_t nfiles;
  size_t size;
  size_t fileid;
  double fp;
  double sum=0;
  file.read((char*)&length,8);
  file.read((char*)&nfiles,8);
  file.close();
 
  cout<<"Length = "<<length<<endl;
  cout<<"Number of files = "<<nfiles<<endl;
  for(size_t i=0;i<nfiles;++i){
    cout<<"Open file "<<i<<endl;
    filename="fp_"+to_string(length)+"_"+to_string(i);
    file.open(filename.c_str(),fstream::in|fstream::binary);   
    file.read((char*)&length,8);
    file.read((char*)&size,8);
    file.read((char*)&fileid,8);
    cout<<"Size = "<<size<<endl;
    for(size_t j=0;j<size;++j){
      file.read((char*)&fp,8);
      sum+=fp;
    }
    file.close();
  }
  cout << setprecision(16)<<endl;
  cout<<"Sum = "<<sum<<endl;
  cout<<"Average = "<<sum/nb_sap[length/2]<<endl;
  cout<<"Coefficient = "<<2*length*sum/pow(4,length)<<endl;
}

