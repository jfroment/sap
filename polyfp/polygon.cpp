#include "polygon.hpp"
#include <list>

//--------------------------
// Polygon::Polygon(string)
//--------------------------
Polygon::Polygon(string code){
  list<string> lstr;
  lstr.push_back("");
  for(size_t ic=0;ic<code.size();++ic){
    char c=code[ic];
    if(c==','){
      lstr.push_back("");
    }
    else{
      lstr.back()+=c;
    }
  }
  length=0;
  int x=0;
  int y=0;
  size_t i=0;
  for(auto it=lstr.begin();it!=lstr.end();++it){
    int v=atoi(it->c_str());
    int av=abs(v);
    if(i%2==0){ //Horizontal step
      x+=v;
      int s=(v<0)?0:1;
      for(size_t j=0;j<av;++j){
	if(length<max_len) step[length]=s;
	++length;
      }
    }
    else{ //Vertical step
      y+=v;
      int s=(v<0)?3:2;
      for(size_t j=0;j<av;++j){
	if(length<max_len) step[length]=s;
	++length;
      }
    }
    ++i;
  }
  closed=((x==0) and (y==0));
}

//------------------------------------------
// Polygon::Polygon(size_t ,size_t,size_t )
//------------------------------------------
Polygon::Polygon(size_t a,size_t b,size_t l){
  length=l;
  if(l<=16){
    for(size_t i=0;i<l;++i){
      step[l-i-1]=a%4;
      a=a/4;
    }
  }
  else{
    for(size_t i=0;i<16;++i){
      step[15-i]=a%4;
      a=a/4;
    }
    for(size_t i=0;i<l-16;++i){
      step[l-1-i]=b%4;
      b=b/4;
    }
  }
  /*  for(size_t i=0;i<l;++i){
    cout<<step[i]<<' ';
  }
  cout<<endl;*/
}
//------------------
// Polygon::display
//------------------

void
Polygon::display() const{
  cout<<"\033[2J";
  int64 x=length/2-2;
  int64 y=length/2+2;
  for(size_t i=0;i<length;++i){
    cout<<"\033["<<y<<";"<<x<<"HX";
    switch(step[i]){
    case 0://left
      --x;
      break;
    case 1://right
      ++x;
      break;
    case 2://up
      --y;
      break;
    case 3://down
      ++y;
      break;
    default:
      assert(false);
      break;
    };
  }
  cout<<"\033["<<length/2+2<<";"<<1<<"H"<<endl;
}

//-------------
// Polygon::fp
//-------------

Reel
Polygon::fp() const{
  map<size_t,InfoVertex> infos;
  size_t id=0;
  int64 x=xinit;
  int64 y=yinit;
  size_t c=pos(x,y);
  for(size_t i=0;i<length;++i){
    size_t l=left(c);
    size_t r=right(c);
    size_t u=up(c);
    size_t d=down(c);
    auto it=infos.find(c);
    if(it==infos.end()){
      infos[c]=InfoVertex(id++,x,y,l,r,u,d);
    }
    else{
      InfoVertex& info=it->second;
      info.v[0]=l;
      info.v[1]=r;
      info.v[2]=u;
      info.v[3]=d;
      info.deg=4;
    }
    it=infos.find(l);
    if(it==infos.end()) infos[l]=InfoVertex(id++,x-1,y);
    it=infos.find(r);
    if(it==infos.end()) infos[r]=InfoVertex(id++,x+1,y);
    it=infos.find(u);
    if(it==infos.end()) infos[u]=InfoVertex(id++,x,y+1);
    it=infos.find(d);
    if(it==infos.end()) infos[d]=InfoVertex(id++,x,y-1);
    switch(step[i]){
    case 0:
      c=l;
      --x;
      break;
    case 1:
      c=r;
      ++x;
      break;
    case 2:
      c=u;
      ++y;
      break;
    case 3:
      c=d;
      --y;
      break;
    default:
      assert(false);
      break;
    };
  }
  size_t ne=infos.size();
  AvxMatrix AvxB;
  AvxMatrix AvxC;
  AvxB.clear();
  AvxC.clear();
  for(auto it=infos.begin();it!=infos.end();++it){
    InfoVertex& info=it->second;
    size_t i=info.id;
    for(size_t k=0;k<info.deg;++k){
      size_t j=infos[info.v[k]].id;
      AvxB.get(i,j)=1;
      AvxB.get(j,i)=1;
    }
    for(auto it2=infos.begin();it2!=infos.end();++it2){
      InfoVertex& info2=it2->second;
      size_t j=info2.id;
      int64 dx=abs(info.x-info2.x);
      int64 dy=abs(info.y-info2.y);
      AvxC.get(i,j)=get_coeff(dx,dy);
    }
  }
  AvxMatrix AvxM;
  AvxM.clear();
  AvxM.from_C_B(AvxC,AvxB,ne);
 
  Reel det=AvxM.Gauss(ne,ne+1);

  Reel res=0;
  for(size_t i=0;i<ne;++i){
    res+=AvxB.get_diag_square_sym(i,ne)*AvxM.get(i,ne);
  }
  res*=(det*0.25);
  return res;
}

//----------------------
// Polygon::next_vertex
//----------------------

/*size_t
Polygon::next_vertex(size_t t,size_t c) const{
  size_t l=left(c);
  size_t r=right(c);
  size_t u=up(c);
  size_t d=down(c);
  size_t s=step_histo[t+1];
  if(grid[l]==t+1 and s==grid_histo[l]) return l;
  if(grid[r]==t+1 and s==grid_histo[r]) return r;
  if(grid[u]==t+1 and s==grid_histo[u]) return u;
  if(grid[d]==t+1 and s==grid_histo[d]) return d;
  assert(false);
  return 0;
  }*/
