#include <string>
#include <tclap/CmdLine.h>
#include <fstream>
#include "coeffs.hpp"
#include "polygon.hpp"

using namespace std;

int main(int argc,char** argv){
  string code;
  string filename;
  bool display;
  try{
    TCLAP::CmdLine cmd("Analyse self avoiding polygon on the square latice given by code or stored on a file.", ' ', "0.1");
    TCLAP::ValueArg<string> codeArg("c","code","Code of a self avoiding polygon",true,"", "code");
    TCLAP::ValueArg<string> fileArg("f","file","File conataining self avoiding polygons",true,"","filename");
    TCLAP::SwitchArg displayFlag("d","dispaly","Display the polygon",cmd,false);
    cmd.xorAdd(codeArg, fileArg);
    cmd.parse(argc, argv);
    code=codeArg.getValue();
    filename=fileArg.getValue();
    display=displayFlag.getValue();
    
  }
  catch (TCLAP::ArgException &e){
    cerr <<"[Error] "<<e.error()<<" for arg "<<e.argId()<<endl;
    return -1;
  }
  compute_coeffs();
  if(code.size()>0){
    Polygon P(code);
    if(P.get_length()>38){
      cerr<<"[Error] The length of the polygon is "<<P.get_length()<<" which is greater than 38."<<endl;
      return -2;
    }
    if(not P.is_closed()){
      cerr<<"[Error] The polygon is not closed"<<endl;
      return -3;
    }
    double fp=P.fp();
    if(display){
      P.display();
      cout<<"lenght = "<<P.get_length()<<endl;
      cout<<"fp = "<<fp<<endl;
    }
    else{
      cout<<fp<<endl;
    }
  }
  else{
    size_t length;
    size_t numbers;
    size_t fileid;
    size_t a;
    size_t b;
    cout<<"Filename = "<<filename<<endl;
    fstream filein;
    fstream fileout;
    filein.open(filename.c_str(),fstream::in|fstream::binary);
    filein.read((char*)&length,8);
    filein.read((char*)&numbers,8);
    filein.read((char*)&fileid,8);
    string filenameout="fp_"+to_string(length)+"_"+to_string(fileid);
    fileout.open(filenameout.c_str(),fstream::out|fstream::binary);
    fileout.write((char*)&length,8);
    fileout.write((char*)&numbers,8);
    fileout.write((char*)&fileid,8);
    cout<<"Length = "<<length<<endl;
    cout<<"Numbers = "<<numbers<<endl;
    cout<<"File id = "<<fileid<<endl;
    for(size_t i=0;i<numbers;++i){
      filein.read((char*)&a,8);
      filein.read((char*)&b,8);
      Polygon P(a,b,length);
      double fp=P.fp();
      fileout.write((char*)&fp,8);
    }
    filein.close();
    fileout.close();
  }
  return 0;

}
