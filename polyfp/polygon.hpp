#ifndef POLYGON_HPP
#define POLYGON_HPP

#include <cstring>
#include <cassert>
#include <stack>
#include <map>

#include "config.hpp"
#include "avx_matrix.hpp"
#include "coeffs.hpp"


using namespace std;

//***********
//* Polygon *
//***********

//! Class for self avoiding polygon in the square lattice.



class Polygon{
private:
  //! Width of the grid
  static const size_t grid_width=max_len-1;
  
  //! Position of the starting point
  static const int64 xinit=max_len/2-2;
  static const int64 yinit=1;
  
  //! Length of the polygon
  size_t length;

  //! Step to produce the polygon left(0) right(1) down(2) up(3)
  size_t step[32];

  //! True is the polygon is closedxs
  bool closed;
public:
  
  //! Construct a polygon from its natural code 2,2,-2-2
  Polygon(string code);

  //! Construct a polygon form steps code
  Polygon(size_t a,size_t b,size_t l);
  
  //! Test if the polygon is closed
  bool is_closed() const;

  //! Return the length of the polygon
  size_t get_length() const;
  
  //! Display the polygon
  void display() const;
 
  //! Return the fp coefficient of the polygon
  Reel fp() const;

  //------------------
  // Static functions
  //------------------
  
  //! Return x coordinate of the case with indice ind.
  //! \param ind indice of the case
  static int64 x(size_t ind);
  
  //! Return y coordinate of the case with indice ind.
  //! \param ind indice of the case
  static int64 y(size_t ind);

  //! Return indice of the case on the left of case with
  //! indice ind.
  //! \param ind indice of the case
  static size_t left(size_t ind);

  //! Return indice of the case on the right of case with
  //! indice ind.
  //! \param ind indice of the case
  static size_t right(size_t ind);
  
  //! Return indice of the case on the up of case with
  //! indice ind.
  //! \param ind indice of the case
  static size_t up(size_t ind);
  
  //! Return indice of the case on the down of case with
  //! indice ind.
  //! \param ind indice of the case
  static size_t down(size_t ind);
  
  //! Return indice of the case at position (x,y).
  //! \param x x-coordinate of the case
  //! \prama y y-coordiante of the case
  static size_t pos(size_t x,size_t y);
};

//**************
//* InfoVertex *
//**************

//! This class is used to obtained the adjacence matrix
//! of the haired polygon.

class InfoVertex{
public:
  //! Indice of the vertex
  size_t id;

  //! Coordinates of the vertex
  int64 x,y;
  
  //! Degree of the vertex
  size_t deg;

  //! Neighbhorhoods of the vertex
  size_t v[4];

  //! Empty constructor (not explicety used)
  InfoVertex();

  //! Constructor for a vertex with not yet detected neighbhors.
  //! \param id indice of the vertex
  //! \param x x-coordinate of the vertex
  //! \param y y-coordinate of the vertex
  InfoVertex(size_t id,int64 x,int64 y);

  //! Constructor for a vertex with four identified neighbhors.
  //! \param id indice of the vertex
  //! \param x x-coordinate of the vertex
  //! \param y y-coordinate of the vertex
  //! \param l indice of the vertex on the left
  //! \param r indice of the vertex on the right
  //! \param u indice of the vertex on the up
  //! \param d indice of the vertex on the dwon
  InfoVertex(size_t id,int64 x,int64 y,size_t l,size_t r,size_t u,size_t d);
};

//********************
//* Inline functions *
//********************

inline bool
Polygon::is_closed() const{
  return closed;
}

inline size_t
Polygon::get_length() const{
  return length;
}

inline size_t
Polygon::left(size_t ind){
  return ind-1;
}

inline size_t
Polygon::right(size_t ind){
  return ind+1;
}

inline size_t
Polygon::up(size_t ind){
  return ind+grid_width;
}

inline size_t
Polygon::down(size_t ind){
  return ind-grid_width;
}

inline int64
Polygon::x(size_t ind){
  return ind%grid_width;
}

inline int64
Polygon::y(size_t ind){
  return ind/grid_width;
}

inline size_t 
Polygon::pos(size_t x,size_t y){
  return  x+y*grid_width;
}

inline
InfoVertex::InfoVertex(){
}

inline
InfoVertex::InfoVertex(size_t _id,int64 _x,int64 _y):
  id(_id),x(_x),y(_y),deg(0){
}

inline
InfoVertex::InfoVertex(size_t _id,int64 _x,int64 _y,size_t l,size_t r,size_t u,size_t d):
  id(_id),x(_x),y(_y),deg(4),v{l,r,u,d}{
}
  
#endif
