#!/bin/sh
filename="oar_fp_array_$1.sh"
mkdir /nfs/home/lmpa/jfromentin/self_avoiding_polygon/output/fp_array_$1
echo "#!/bin/sh">$filename
echo "#OAR -l core=1,walltime=01:00:00">>$filename
echo "#OAR -t idempotent">>$filename
echo "#OAR -q besteffort">>$filename
echo "#OAR -t besteffort">>$filename
echo "#OAR -p cputype=\"skylake\"">>$filename
echo "#OAR --array-param-file /nfs/home/lmpa/jfromentin/self_avoiding_polygon/output/array_$1">>$filename
echo "#OAR -O /nfs/home/lmpa/jfromentin/self_avoiding_polygon/output/fp_array_$1/job.%jobid%.output">>$filename
echo "#OAR -E /nfs/home/lmpa/jfromentin/self_avoiding_polygon/output/fp_array_$1/job.%jobid%.error">>$filename
echo "echo \"Change direcory\"">>$filename
echo "cd /nfs/home/lmpa/jfromentin/self_avoiding_polygon/output">>$filename
echo "echo \$PWD">>$filename
echo "echo \"Running ...\"">>$filename
echo "time ../bin/polyfp \$*">>$filename
echo "echo \"done\"">>$filename
chmod +x $filename
