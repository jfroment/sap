#!/bin/sh
filename="oar_sapgen_$1.sh"
echo "#!/bin/sh">$filename
echo "#OAR -l core=1,walltime=01:00:00">>$filename
echo "#OAR -t idempotent">>$filename
echo "#OAR -q besteffort">>$filename
echo "#OAR -t besteffort">>$filename
echo "#OAR -p cputype=\"skylake\"">>$filename
echo "echo \"Change direcory\"">>$filename
echo "cd /nfs/home/lmpa/jfromentin/self_avoiding_polygon/output">>$filename
echo "echo \$PWD">>$filename
echo "echo \"Running ...\"">>$filename
echo "time ../bin/sapgen -l $1 -s 10000000 \$*">>$filename
echo "echo \"done\"">>$filename
chmod +x $filename
